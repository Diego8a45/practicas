<?php

namespace App\Http\Controllers;

use App\Models\MiCarrito;
use App\Models\misCompras;
use App\Models\Producto;
use Illuminate\Http\Request;

class MiCarritoController extends Controller
{
    public function carrito()
    {
        $carro = MiCarrito::all()->where('mi_id', Session('usuario')->id);
        if (count($carro) >= 1) {
            return view('carrito', ["carro" => $carro]);
        } else {
            return view("perfil", ["estatus" => "error", "mensaje" => "Tu carrito esta Vacio D:"]);
        }
    }

    public function almacen(request $datos)
    {
        $nombre = $datos->nombrep;
        $producto = Producto::where('id', $nombre)->first();
        if ($producto) {
            if ($producto->cantidad == 0) {
                return view("carrito", ["estatus" => "error", "mensaje" => "Lo sentimos este producto se agoto"]);
            } else {
                $id = Session('usuario')->id;
                $nombreProducto = $producto->nombre_producto;
                $descripcion = $producto->descripcion;
                $precio = $producto->precio;
                $ruta = $producto->imagen1;
                $carrito = new MiCarrito();
                $carrito->mi_id = $id;
                $carrito->producto = $nombreProducto;
                $carrito->descripcion = $descripcion;
                $carrito->precio = $precio;
                $carrito->ruta = $ruta;
                $carrito->save();
                return view("perfil", ["estatus" => "success", "mensaje" => "Se subio al carrito :D"]);
            }
        } else {
            return view("perfil", ["estatus" => "error", "mensaje" => "Error al subir al carrito D:"]);
        }
    }

    public function eliminarproducto(request $datos)
    {
        $idcarrito = $datos->id;
        $carro = MiCarrito::find($idcarrito);
        if ($carro) {
            $carro->delete();
            return view("perfil", ["estatus" => "success", "mensaje" => "Se elimino Correctamente del carrito"]);
        } else {
            return view("perfil", ["estatus" => "error", "mensaje" => "No se elimino el producto del carrito"]);
        }

    }

    public function comprarCarrito(request $datos)
    {
        $id = $datos->id;

        $carro = MiCarrito::where('mi_id', $id);
        if ($carro) {
            $compras = new misCompras();
            $compras->mi_id = $id;
            $compras->producto = $datos->nombre;
            $compras->descripcion = $datos->descripcion;
            $compras->precio = $datos->precio;
            $compras->ruta = "adios";
            $compras->save();
            $carro->delete();
            return view("perfil", ["estatus" => "success", "mensaje" => "Se compro el carrito"]);
        } else {
            return view("perfil", ["error" => "success", "mensaje" => "No se compro el carrito"]);
        }
    }

}
