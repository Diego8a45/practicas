<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class ProductoController extends Controller
{
    public function registrarProducto()
    {
        return view('publicarProducto');
    }

    public function ver()
    {
        return view('productos');
    }

    public function producto(request $datos)
    {
        $id_vendedor = Session('usuario')->id;
        $nombre = $datos->nombre;
        $precio = $datos->precio;
        $descripcion = $datos->des;
        $piezas = $datos->piezas;
        $tamaño = $datos->tamaño;
        $categoria = $datos->categoria;
        $estatus = $datos->estatus;
        $color = $datos->color;
        $imagen1 = $datos->file('imagen1')->store('public/images');
        $imagen2 = $datos->file('imagen2')->store('public/images');
        $url1 = Storage::url($imagen1);
        $url2 = Storage::url($imagen2);
        $producto = new Producto();
        $producto->id_vendedor = $id_vendedor;
        $producto->nombre_producto = $nombre;
        $producto->precio = $precio;
        $producto->descripcion = $descripcion;
        $producto->cantidad = $piezas;
        $producto->categoria = $categoria;
        $producto->estatus = $estatus;
        $producto->color = $color;
        $producto->imagen1 = $url1;
        $producto->imagen2 = $url2;
        $producto->save();
        return view("publicarProducto", ["estatus" => "success", "mensaje" => "¡Producto Registrado Exitosamente!"]);
    }

    public function misProductos()
    {
        $miproducto = Producto::all()->where('id_vendedor', Session('usuario')->id);
        return view('misproductos', ["miproducto" => $miproducto]);

    }

    public function eliminar(request $datos)
    {
        $id = $datos->idBuscado;
        $producto = Producto::find($id);
        if ($producto) {
            $producto->delete();
            return view("publicarProducto", ["estatus" => "success", "mensaje" => "¡Producto Eliminado Exitosamente!"]);
        } else {
            return view("publicarProducto", ["estatus" => "error", "mensaje" => "¡Uppps! Hubo un problema intentelo mas tarde"]);
        }
    }

    public function mostrarTodos()
    {
        $todo = Producto::all();
        if (Session()->has('usuario')) {
            return view('productos', ["todo" => $todo]);
        } else {
            return view('tienda', ["todo" => $todo]);
        }
    }
}
