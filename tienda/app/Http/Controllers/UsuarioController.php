<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UsuarioController extends Controller
{
    public function inicio()
    {
        return view('inicio');
    }

    public function registrar()
    {
        return view('registro');
    }

    public function login()
    {
        return view('login');
    }

    public function editar()
    {
        if (session('usuario')->role_id == 1) {
            return view('editarPerfil');
        } else {
            return view('editarAdmin');
        }
    }

    public function perfil()
    {

        if (session('usuario')->role_id == 1) {
            return view('perfil');
        } else {
            return view('perfilAdmin');
        }
    }

    public function compras()
    {
        return view('misCompras');
    }

    public function registrarUsuario(request $datos)
    {
        $usuario = Usuario::where('correo', $datos->email)->first();
        if ($usuario) {
            return view("registro", ["estatus" => "error", "mensaje" => "¡El correo ya se encuentra registrado!"]);
        }

        $nombres = $datos->nombre;
        $app = $datos->app;
        $apm = $datos->apm;
        $email = $datos->email;
        $rol = 1;
        $password = $datos->password;
        $validar = $datos->validar;

        if ($password != $validar) {
            return view("registro", ["estatus" => "error", "mensaje" => "¡Las contraseñas son diferentes!"]);
        }

        $usuario = new usuario();

        $usuario->nombres = $nombres;
        $usuario->apellido_paterno = $app;
        $usuario->apellido_materno = $apm;
        $usuario->correo = $email;
        $usuario->password = bcrypt($password);
        $usuario->role_id = $rol;

        $usuario->save();
        return view("registro", ["estatus" => "success", "mensaje" => "¡Cliente Registrado Exitosamente!"]);
    }

    public function VerificarUsuario(Request $datos)
    {

        $usuario = Usuario::where('correo', $datos->email)->first();
        if (!$usuario) {
            return view("login", ["estatus" => "error", "mensaje" => "¡Correo incorrecto!"]);
        }

        if (!Hash::check($datos->password, $usuario->password)) {
            return view("login", ["estatus" => "error", "mensaje" => "¡Contraseña incorrecto!"]);
        }

        Session::put('usuario', $usuario);
        if (isset($datos->url)) {
            $url = decrypt($datos->url);
            return redirect($url);
        } else {
            return redirect()->route('usuario.perfil');
        }
    }

    public function cerrarSesion()
    {
        if (Session::has('usuario')) {
            Session::forget('usuario');
        }
        return redirect()->route('inicio');
    }

    public function cambios(request $datos)
    {
        $id = Session('usuario')->id;
        $usuario = Usuario::find($id);
        if ($usuario) {
            $usuario->nombres = $datos->nombre;
            $usuario->apellido_paterno = $datos->app;
            $usuario->apellido_materno = $datos->apm;
            $usuario->correo = $datos->email;
            $usuario->save();
            if (session('usuario')->role_id == 1) {
                return view("editarPerfil", ["estatus" => "success", "mensaje" => "¡Datos Actualizados Exitosamente!"]);
            } else {
                return view("editarAdmin", ["estatus" => "success", "mensaje" => "¡Datos Actualizados Exitosamente!"]);
            }
        }

    }

}
