@extends('layout.layout')

@section('titulo')
    <title>Registrarme como Cliente</title>
@endsection

@section('css')
    <link href="/vendor/inicio.css" rel="stylesheet">


@endsection

@section('contenido')
    <div class="card bg-dark col-9 mt-5 mx-auto text-white">
        <h1 class="display-5 fw-normal text-center mt-5">REGISTRO CLIENTE</h1>
        <form method="post" action="{{route('registrar.form')}}" class="mx-5">
            {{csrf_field()}}
            <label class="text-danger">
                @if(isset($estatus))
                    @if($estatus == "success")
                        <label class="text-success">{{$mensaje}}</label>
                    @endif
                    @if($estatus == "error")
                        <label class="text-danger">{{$mensaje}}</label>
                    @endif
                @endif
            </label>
            <div class="row">
                <div class="col-md-4 mb-3">
                    <label for="nombre">Nombre(s):</label>
                    <input type="text" class="form-control" name="nombre" placeholder="" value="" required>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="lastName">Apellido Paterno</label>
                    <input type="text" class="form-control" name="app" placeholder="" value="" required>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="lastName">Apellido Materno</label>
                    <input type="text" class="form-control" name="apm" placeholder="" value="" required>
                </div>
            </div>
            <div class="mb-3">
                <label for="email">Correo</label>
                <input type="email" class="form-control" name="email" placeholder="tunombre@ejemplo.com" required>
            </div>
            <div class="mb-3">
                <label for="address2">Contraseña</label>
                <input type="password" class="form-control" name=password placeholder="" required>
            </div>
            <div class="mb-3">
                <label for="address2">Verificar contraseña</label>
                <input type="password" class="form-control" name="validar" placeholder="" required>
            </div>
            <div class="text-center">
                <a class="small" href="">¿Tienes una cuenta? ¡Inicia Sesión!</a>
            </div>
            <input type="submit" class="link w-100 btn btn-warning btn-lg mb-5" type="submit" value="Crear Cuenta">
        </form>

    </div>
@endsection

@section('js')
@endsection
