@extends('layout.layout2')

@section('titulo')
    <title>Mis productos</title>
@endsection

@section('css')
    <link href="/vendor/inicio.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap5.min.css">



@endsection

@section('contenido')
    <div class="card col-12 mx-auto mt-5 ">
        <table id="producto" class="table table-striped">
            <thead class="bg-dark text-center text-white">
            <tr>
                <th>id</th>
                <th>Producto</th>
                <th>Precio</th>
                <th>cantidad</th>
                <th>Imagen</th>
            </tr>
            </thead>
            <tbody class="bg-warning text-center">
            @foreach($miproducto as $product)
                <tr>
                    <td>{{$product->id}}</td>
                    <td>{{$product->nombre_producto}}</td>
                    <td>{{$product->precio}}</td>
                    <td>{{$product->cantidad}}</td>
                    <td><img src="{{$product->imagen1}}" alt="{{$product->nombre_producto}}" class="col-3"></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <form method="post" action="{{route('usuario.misProductos.form')}}">
        {{csrf_field()}}
        <div class="mb-3 mt-2 d-flex col-6 mx-auto align-items-center">
            <label for="address2" class="mx-2">Eliminar Producto</label>
            <input type="text" class="form-control w-25 mx-2" name="idBuscado" placeholder="id del producto" required>
            <input type="submit" class="link w-25 btn btn-danger btn-lg mx-2" type="submit" value="Eliminar">
        </div>
    </form>
    <div class="col-6 mx-auto text-center">
        @if(isset($estatus))
            @if($estatus == "success")
                <label class="text-success">{{$mensaje}}</label>
            @endif
            @if($estatus == "error")
                <label class="text-danger">{{$mensaje}}</label>
            @endif
        @endif
    </div>
@endsection

@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#producto').DataTable();
        });
    </script>
@endsection
