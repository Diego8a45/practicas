<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link href="/css/sb-admin-2.css" rel="stylesheet">
    @yield('titulo')
    @yield('css')
</head>
<body style="background: white">
<header class="p-3 bg-dark text-white">
    <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
        <a href="/" class="text-start d-flex mb-2 mb-lg-0 text-white text-decoration-none fs-2 mx-4">
            Armazon.com
        </a>

        <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
            <li><a href="{{route('productos')}}" class="nav-link px-2 text-white">Productos</a></li>
            <li><a href="{{route('usuario.carrito')}}" class="nav-link px-2 text-white">Mi carrito</a></li>
            <li><a href="#" class="nav-link px-2 text-white">historial</a></li>
        </ul>

        <form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3">
            <input type="search" class="form-control form-control-dark" placeholder="buscar...">
        </form>

        <div class="text-end">
            <a href="{{route('registrar')}}" class="btn btn-outline-light me-2">Registrarme</a>
            <a href="{{route('login')}}" class="btn btn-warning">Iniciar Sesión</a>
        </div>
    </div>
</header>
@yield('contenido')
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js"
        integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"
        integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc"
        crossorigin="anonymous"></script>
@yield('js')
</body>
</html>
