@extends('layout.layout2')

@section('titulo')
    <title>Publicar Producto</title>
@endsection

@section('css')
    <link href="/vendor/inicio.css" rel="stylesheet">


@endsection

@section('contenido')
    <div class="card bg-dark col-9 mt-5 mx-auto text-white">
        <h1 class="display-5 fw-normal text-center mt-5">PUBLICAR ARTICULO</h1>
        <form method="post" action="{{route('usuario.publicar.form')}}" class="mx-5" enctype="multipart/form-data">
            {{csrf_field()}}
            <label class="text-danger">
                @if(isset($estatus))
                    @if($estatus == "success")
                        <label class="text-success">{{$mensaje}}</label>
                    @endif
                    @if($estatus == "error")
                        <label class="text-danger">{{$mensaje}}</label>
                    @endif
                @endif
            </label>
            <div class="mb-3 mt-4">
                <label for="nombre">Nombre del producto</label>
                <input type="text" class="form-control" name="nombre" required>
            </div>
            <div class="col-12 mt-4">
                <label for="username" class="form-label">Precio</label>
                <div class="input-group has-validation">
                    <span class="input-group-text">$</span>
                    <input type="text" class="form-control" name="precio" required>
                </div>
            </div>
            <div class="mb-3 mt-4">
                <label for="nombre">Descripcion del producto</label>
                <input type="text" class="form-control" name="des" required>
            </div>
            <div class="col-12 mt-4">
                <label for="username" class="form-label">Cantidad</label>
                <div class="input-group has-validation">
                    <input type="text" class="form-control" name="piezas" required>
                    <span class="input-group-text">PZAS.</span>
                </div>
            </div>
            <div class="mb-3 mt-4">
                <label for="tamaño">Tamaño</label>
                <input type="text" class="form-control" name="tamaño" placeholder="15cmX15cm, 5cm, M, T16" required>
            </div>
            <div class="row">
                <div class="col-md-6 mt-4">
                    <label for="carrera" class="form-label">Categoria</label>
                    <select class="form-select" name="categoria" required>
                        <option value="">Elige...</option>
                        <option value="Tecnologia">Tecnología</option>
                        <option value="Autos">Autos</option>
                        <option value="Hogar">Hogar</option>
                        <option value="Despensa">Despensa</option>
                        <option value="Moda">Moda</option>
                        <option value="Electrodomesticos">Electrodomésticos</option>
                        <option value="Belleza">Belleza</option>
                        <option value="Deportes">Deportes</option>
                    </select>
                </div>
                <div class="col-md-6 mt-4">
                    <label for="carrera" class="form-label">Estatus</label>
                    <select class="form-select" name="estatus" required>
                        <option value="">Elige...</option>
                        <option value="Nuevo">Nuevo</option>
                        <option value="Usado">Usado</option>
                    </select>
                </div>
            </div>
            <div class="mb-3 mt-4">
                <label for="tamaño">Color</label>
                <input type="text" class="form-control" name="color" placeholder="" required>
            </div>
            <div class="row">
                <div class="mb-3 mt-4 col-6">
                    <label for="tamaño">Imagenes:</label>
                    <input type="file" class="form-control" name="imagen1" placeholder="" required>
                </div>
                <div class="mb-3 mt-4 col-6">
                    <label for="tamaño"></label>
                    <input type="file" class="form-control" name="imagen2" placeholder="" required>
                </div>
            </div>
            <input type="submit" class="link w-100 btn btn-warning btn-lg mb-5 mt-5" type="submit"
                   value="Publicar Producto">
        </form>

    </div>
@endsection

@section('js')
@endsection
