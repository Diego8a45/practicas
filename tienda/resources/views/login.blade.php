@extends('layout.layout')

@section('titulo')
    <title>Armazon.com</title>
@endsection

@section('css')
    <link href="/vendor/inicio.css" rel="stylesheet">


@endsection

@section('contenido')
    <div class="card bg-dark col-5 mt-5 mx-auto text-white">
        <h1 class="display-5 fw-normal text-center mx-4 my-3">Bienvenido a Armazon.com</h1>
        <form method="post" action="{{route('login.form')}}" class="mx-5">
            {{csrf_field()}}
            <label class="text-danger">
                @if(isset($estatus))
                    <label class="text-danger">{{$mensaje}}</label>
                @endif
            </label>
            <div class="row">
                <div class="mb-3">
                    <label for="email">Correo</label>
                    <input type="email" class="form-control" name="email" placeholder="tunombre@ejemplo.com" required>
                </div>
                <div class="mb-3">
                    <label for="address2">Contraseña</label>
                    <input type="password" class="form-control" name=password placeholder="" required>
                </div>
                <div class="text-center">
                    <a class="small text-white" href="{{route('registrar')}}">¿No tienes cuenta? ¡Registrate!</a>
                </div>
                <input type="submit" class="link col-5 mt-3 mx-auto btn btn-warning btn-lg mb-5" type="submit"
                       value="Iniciar Sesión">
            </div>
        </form>
    </div>
@endsection

@section('js')
@endsection
