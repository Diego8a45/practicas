@extends('layout.layout1')

@section('titulo')
    <title>{{Session('usuario')->nombres}}</title>
@endsection

@section('css')
    <link href="/vendor/inicio.css" rel="stylesheet">
@endsection

@section('contenido')

    <div class="card col-11 mx-auto my-3 bg-light">
        @if(isset($estatus))
            @if($estatus == "success")
                <label class="text-success text-center mt-3">{{$mensaje}}</label>
            @endif
            @if($estatus == "error")
                <label class="text-danger text-center mt-3">{{$mensaje}}</label>
            @endif
        @endif
        <div class="card-header mt-5  bg-light align-items-center border-0">
            <div class="mx-auto col-3">
                <img src="{{asset('images/autos.jpg')}}" class="col-12 h-100 rounded-pill" alt="">
            </div>
        </div>
        <div class="card-body  bg-light">
            <h3 class="display-7 fw-normal text-center mx-4 my-3">{{Session('usuario')->nombres}} {{Session('usuario')->apellido_paterno}} {{Session('usuario')->apellido_materno}}</h3>
        </div>
        <div class="card-body col-3 mx-auto bg-light text-center d-flex">
            <a href="{{route('usuario.editar')}}"
               class="link col-lg-5 mx-auto btn btn-warning col-sm-12 mb-5">Editar</a>
            <a href="{{route('productos')}}" class="link col-lg-5 col-sm-12 mx-auto btn btn-success mb-5">Comprar</a>
        </div>
        <div class="border-bottom"></div>
        <div class="card-body  bg-light">
            <h5 class="display-7 fw-normal mx-1 my-1"><a href="" class="text-decoration-none text-dark">Mis pedidos</a>
            </h5>
            <h6 class="display-7 fw-normal mx-4 my-2"><a href="" class="text-decoration-none text-dark">Da click aquí
                    para poder ver todos tus pedidos y el estatus de ellos.</a></h6>
        </div>
        <div class="border-bottom"></div>
        <div class="card-body  bg-light">
            <h5 class="display-7 fw-normal mx-1 my-1"><a href="" class="text-decoration-none text-dark">Mi Carrito</a>
            </h5>
            <h6 class="display-7 fw-normal mx-4 my-2"><a href="" class="text-decoration-none text-dark">Da click aquí
                    para poder ver todos tus pedidos pwndientes por comprar.</a></h6>
        </div>
    </div>
@endsection

@section('js')
@endsection
