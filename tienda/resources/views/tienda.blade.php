@extends('layout.layout')

@section('titulo')
    <title>Tienda</title>
@endsection

@section('css')

@endsection

@section('contenido')
    <div class="container mt-5 bg-dark">
        <div class="row px-4">
            @foreach($todo as $tod)
                <div class="card col-lg-4  mt-5">
                    <div class="card-header">
                        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img src="{{$tod->imagen1}}" class="d-block w-100" alt="{{$tod->nombre_producto}}">
                                </div>
                                <div class="carousel-item">
                                    <img src="{{$tod->imagen2}}" class="d-block w-100" alt="{{$tod->nombre_producto}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <h3 class="text-center">{{$tod->nombre_producto}}</h3>
                        <h6>{{$tod->descripcion}}</h6>
                        <h5>{{$tod->estatus}}</h5>
                        <h5>${{$tod->precio}}</h5>
                        <div class="d-flex justify-content-between align-items-center mt-5">
                            <div class="btn-group">
                                <form method="post" action="{{route('usuario.carrito.form')}}">
                                    {{csrf_field()}}
                                    <input type="hidden" value="{{$tod->id}}" name="nombrep">
                                    <input type="submit" class="btn btn-sm btn-warning" value="🛒">
                                    <a href="" class="btn btn-sm btn-success">$</a>
                                </form>
                            </div>
                            <small class="text-muted">{{$tod->cantidad}} pza(s).</small>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>


        <div class="row ">
            <ul class="pagination mt-5 col-5 mx-3 ">
                <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item active" aria-current="page">
                    <a class="page-link" href="#">2</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#">Next</a>
                </li>
            </ul>

        </div>
    </div>
@endsection

@section('js')
@endsection
