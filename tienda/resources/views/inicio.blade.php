@extends('layout.layout')

@section('titulo')
    <title>Armazon.com</title>
@endsection

@section('css')
    <link href="/vendor/inicio.css" rel="stylesheet">
@endsection

@section('contenido')
    <div id="carouselExampleCaptions" class="carousel  slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active"
                    aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"
                    aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"
                    aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner item-center">
            <div class="carousel-item active">
                <img src="{{asset('images/portada.jpg')}}" class="imagen d-block w-100" alt="">
                <div class="carousel-caption d-none d-md-block">
                </div>
            </div>
            <div class="carousel-item">
                <img src="{{asset('images/portada2.jpg')}}" class="imagen d-block w-100" alt="">
                <div class="carousel-caption d-none d-md-block">
                </div>
            </div>
            <div class="carousel-item">
                <img src="{{asset('images/portada1.jpg')}}" class="imagen d-block w-100" alt="">
                <div class="carousel-caption d-none d-md-block">
                </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions"
                data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions"
                data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
        </button>
    </div>
    <div class="col-12 mt-5">
        <div class="d-lg-flex">
            <div class="card border-0 col-lg-4 col-md-12 my-4 mx-lg-auto">
                <div class="card-header border-0 bg-light text-center">
                    <h1 class="display-4 fw-normal">Moda</h1>
                    <p class="lead fw-normal">45% de descuento en todas las prendas.</p>
                </div>
                <div class="card-body bg-light">
                    <img src="{{asset('images/moda.jpg')}}" class="card-img-top" alt="">
                </div>
            </div>
            <div class="card col-lg-4 col-md-12 my-4 mx-lg-auto">
                <div class="card-header bg-dark text-white text-center">
                    <h1 class="display-4 fw-normal">Tecnología</h1>
                    <p class="lead fw-normal">Las mejores marcas y la mejor calidad solo aqui.</p>
                </div>
                <div class="card-body bg-dark">
                    <img src="{{asset('images/tecnologia.jpg')}}" class="card-img-top" alt="">
                </div>
            </div>
            <div class="card col-lg-4 border-0  col-md-12 my-4 mx-lg-auto">
                <div class="card-header border-0 bg-light text-center">
                    <h1 class="display-5 fw-normal">Electrodomésticos</h1>
                    <p class="lead fw-normal">Hasta el 35% de descuento en diversidad de nuestros productos.</p>
                </div>
                <div class="card-body bg-light">
                    <img src="{{asset('images/electro.jpg')}}" class="card-img-top" alt="">
                </div>
            </div>
        </div>
        <div class="d-lg-flex">
            <div class="card border-0 col-lg-4 col-md-12 my-4 mx-lg-auto">
                <div class="card-header border-0 bg-light text-center">
                    <h1 class="display-4 fw-normal">Hogar</h1>
                    <p class="lead fw-normal">Lo mejor en arreglos y decoraciones para el hogar.</p>
                </div>
                <div class="card-body bg-light">
                    <img src="{{asset('images/hogar.jpg')}}" class="card-img-top" alt="">
                </div>
            </div>
            <div class="card col-lg-4 col-md-12 my-4 mx-lg-auto">
                <div class="card-header bg-dark text-white text-center">
                    <h1 class="display-4 fw-normal">Auto</h1>
                    <p class="lead fw-normal">Lo mejor en autos y las mejores marcas solo aquí.</p>
                </div>
                <div class="card-body bg-dark">
                    <img src="{{asset('images/autos.jpg')}}" class="card-img-top" alt="">
                </div>
            </div>
            <div class="card col-lg-4 border-0  col-md-12 my-4 mx-lg-auto">
                <div class="card-header border-0 bg-light text-center">
                    <h1 class="display-5 fw-normal">Despensa</h1>
                    <p class="lead fw-normal">Diversidad de productos para el hogar solo aquí.</p>
                </div>
                <div class="card-body bg-light">
                    <img src="{{asset('images/despensa.jpg')}}" class="card-img-top" alt="">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
@endsection
