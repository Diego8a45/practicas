@extends('layout.layout2')

@section('titulo')
    <title>{{Session('usuario')->nombres}}</title>
@endsection

@section('css')
    <link href="/vendor/inicio.css" rel="stylesheet">
@endsection

@section('contenido')
    <div class="card col-11 mx-auto my-3 bg-light">
        <div class="card-header  bg-light align-items-center border-0">
            <div class="mx-auto col-3">
                <img src="{{asset('images/autos.jpg')}}" class="col-12 h-100 rounded-pill" alt="">
            </div>
        </div>
        <div class="card-body  bg-light">
            <h3 class="display-7 fw-normal text-center mx-4 my-3">{{Session('usuario')->nombres}} {{Session('usuario')->apellido_paterno}} {{Session('usuario')->apellido_materno}}</h3>
        </div>
        <div class="card-body col-3 mx-auto bg-light text-center d-flex">
            <a href="{{route('usuario.editar')}}"
               class="link col-lg-5 mx-auto btn btn-warning col-sm-12 mb-5">Editar</a>
            <a href="" class="link col-lg-5 col-sm-12 mx-auto btn btn-success mb-5">Ventas</a>
        </div>
        <div class="border-bottom"></div>
        <div class="card-body  bg-light">
            <a href="{{route('usuario.misProductos')}}" class="text-decoration-none text-dark">
                <h5 class="display-7 fw-normal mx-1 my-1">Mis productos Publicados</h5>
                <h6 class="display-7 fw-normal mx-4 my-2">Da click aquí para poder ver todos tus productos
                    publicados.</h6>
            </a>
        </div>
        <div class="border-bottom"></div>
        <div class="card-body  bg-light">
            <a href="{{route('usuario.misProductos')}}" class="text-decoration-none text-dark">
                <h5 class="display-7 fw-normal mx-1 my-1">Publicar Producto</h5>
                <h6 class="display-7 fw-normal mx-4 my-2">Da click aquí para poder publicar un nuevo producto.</h6>
            </a>
        </div>
    </div>
@endsection

@section('js')
@endsection
