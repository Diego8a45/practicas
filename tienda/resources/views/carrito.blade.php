@extends('layout.layout1')
@section('titulo')
    <title>Tienda</title>
@endsection

@section('css')

@endsection

@section('contenido')
    <div class="container bg-dark pb-5">
        @if(isset($estatus))
            @if($estatus == "success")
                <label class="text-success">{{$mensaje}}</label>
            @endif
            @if($estatus == "error")
                <label class="text-danger">{{$mensaje}}</label>
            @endif
        @endif

        <div class="mt-5">
            <h2 class="text-center text-white">MI CARRITO</h2>
        </div>
        <table id="producto" class="table table-striped">
            <thead class="bg-dark text-center text-white">
            <tr>
            </tr>
            </thead>
            <tbody class="bg-light">
            @foreach($carro as $car)
                <tr>
                    <td><img src="{{$car->ruta}}" alt="" class="col-lg-3 col-md-4 col-9"></td>
                    <td>
                        <h3>{{$car->producto}}</h3>
                        <h5>{{$car->descripcion}}</h5>
                    </td>
                    <td class="text-center">
                        <h5>${{$car->precio}}</h5>
                    </td>
                    <td>
                        <form method="post" action="{{route('usuario.elimina')}}">
                            {{csrf_field()}}
                            <input type="hidden" value="{{$car->id}}" name="id">
                            <input type="submit" class="btn-none border-0 text-danger" value="Eliminar">
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="col-12 mx-auto text-white d-flex bg-light">
            <div class="col-12"><h3 class="text-center text-dark">Total: {{$car->sum('precio')}}</h3></div>
        </div>
        <div class="col-6 mx-auto">
            <form method="post" action="{{route('usuario.comprarCarrito')}}">
                {{csrf_field()}}
                @foreach($carro as $car)
                    <input type="hidden" value="{{$car->mi_id}}" name="id">
                    <input type="hidden" value="{{$car->producto}}" name="nombre">
                    <input type="hidden" value="{{$car->descripcion}}" name="descripcion">
                    <input type="hidden" value="{{$car->precio}}" name="precio">
                @endforeach
                <input type="submit" class="link col-12 btn btn-warning btn-lg mt-5" type="submit" value="Comprar">
            </form>
        </div>
    </div>
@endsection
<script>
</script>
@section('js')
@endsection
