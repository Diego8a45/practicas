<?php

use App\Http\Controllers\MiCarritoController;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\UsuarioController;
use Illuminate\Support\Facades\Route;

/*
 *
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [UsuarioController::class, 'inicio'])->name('inicio');
Route::get('/cerrarSesion', [UsuarioController::class, 'cerrarSesion'])->name('cerrar.sesion');

Route::get('/registrarUsuario', [UsuarioController::class, 'registrar'])->name('registrar');
Route::post('/registrarUsuario', [UsuarioController::class, 'registrarUsuario'])->name('registrar.form');
Route::get('/IniciarSesion', [UsuarioController::class, 'login'])->name('login');
Route::post('/IniciarSesion', [UsuarioController::class, 'VerificarUsuario'])->name('login.form');
Route::get('/Productos', [ProductoController::class, 'mostrarTodos'])->name('productos');

Route::prefix('/usuario')->middleware("Verificar")->group(function () {
    Route::get('/IniciarSesion', [UsuarioController::class, 'perfil'])->name('usuario.perfil');
    Route::get('/VenderProducto', [ProductoController::class, 'registrarProducto'])->name('usuario.publicar');
    Route::post('/VenderProducto', [ProductoController::class, 'producto'])->name('usuario.publicar.form');
    Route::get('/MisProductosPublicados', [ProductoController::class, 'misProductos'])->name('usuario.misProductos');
    Route::post('/MisProductosPublicados', [ProductoController::class, 'eliminar'])->name('usuario.misProductos.form');
    Route::get('/carrito', [MiCarritoController::class, 'carrito'])->name('usuario.carrito');
    Route::post('/productos', [MiCarritoController::class, 'almacen'])->name('usuario.carrito.form');
    Route::post('/carrito', [MiCarritoController::class, 'eliminarproducto'])->name('usuario.elimina');
    Route::post('/compraRealizada', [MiCarritoController::class, 'comprarCarrito'])->name('usuario.comprarCarrito');
    Route::get('/editarDatos', [UsuarioController::class, 'editar'])->name('usuario.editar');
    Route::get('/misCompras', [UsuarioController::class, 'compras'])->name('usuario.compras');
    Route::post('/editarDatos', [UsuarioController::class, 'cambios'])->name('usuario.editar.form');

});


